program lab3_3;


const
  Size = 5;

type
  LongWorkTime = record
    TaskNumber: Integer;
    WorkTime: Real;
  end;
  TypeOfLongWorkTime = array [0..1000] of LongWorkTime;

var
  Lengths: array [1..1000] of String[Size];
  LongWorks: TypeOfLongWorkTime;
  Quantity: integer;
  i, j, HoursFromMinutes: Integer;
  Time: String;
  Hours, Minutes: Integer;
  AllTime, TaskTime, Weeks: Real;

function CorrectnessOfLine(time: String): Boolean;
begin
  if (length(time) = Size) then 
  begin
    if ((time[1] >= '0') and (time[1] <= '9')) and ((time[2] >= '0') and (time[2] <= '9'))
        and (time[3] = ':') and ((time[4] >= '0') and (time[4] <= '9')) and ((time[5] >= '0') and (time[5] <= '9')) then
      CorrectnessOfLine := True;
  end
  else 
    CorrectnessOfLine := False;
end;

procedure CountHours(var hours: Integer; time: String);
begin
  hours := (ord(time[1]) - 48) * 10 + (ord(time[2]) - 48);
end;

procedure CountMinutes(var minutes: Integer; time: String);
begin
  minutes := (ord(time[4]) - 48) * 10 + (ord(time[5]) - 48);
end;

procedure ConvertMinutesInToHours(var minutes, hoursCounter: Integer);
begin
  while (minutes > 60) do
  begin
    minutes := minutes mod 60;
    hoursCounter := hoursCounter + 1;
  end;
end;

procedure SaveInformationAboutLongTask(taskTime: Real; var longWorks: TypeOfLongWorkTime; i, j: Integer);
begin
  with longworks[j] do 
  begin
    TaskNumber := i;
    WorkTime := taskTime;
  end;
end;

procedure RoundWeeks(var weeks: real);
begin
  if (Weeks <> trunc(Weeks)) then Weeks := trunc(Weeks) + 1;
end;

begin
  Write('Enter quantity of tasks >> ');
  ReadLn(Quantity);
  i := 1;
  j := 0;
  AllTime := 0;
  while(i <= Quantity) do
  begin
    Write('Enter length of task number ', i, ' as time (hh:mm)  >> ');
    ReadLn(Lengths[i]);
    Time := Lengths[i];
    if CorrectnessOfLine(Time) = True then
    begin
      CountHours(Hours, Time);
      CountMinutes(Minutes, Time);
      ConvertMinutesInToHours(Minutes, HoursFromMinutes);
      AllTime := AllTime + Hours + HoursFromMinutes + Minutes / 60;
      TaskTime := Hours + HoursFromMinutes + Minutes / 60;
      HoursFromMinutes := 0;
      if (TaskTime > 8) then
      begin
        SaveInformationAboutLongTask(TaskTime, LongWorks, i, j);
        j := j + 1;
      end;
      i := i + 1;
      WriteLn('DONE!');
    end
    else
    begin
      WriteLn('ERROR!');
    end;
  end;
  WriteLn(AllTime:0:1, ' hour(s)');
  Weeks := AllTime / 40;
  WriteLn();
  RoundWeeks(Weeks);
  Writeln('You need ', Weeks, ' weeks');
  WriteLn();
  for i := 0 to j - 1 do
  begin
    Writeln('Task number ', LongWorks[i].TaskNumber, ' needs ', LongWorks[i].WorkTime:0:2, ' hours');
  end;
end.